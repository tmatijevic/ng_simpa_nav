(function(){

var defaults = {
  headerWrapperSelector: '.ng-simpa-navigation-header',
  navbar: '#ng-simpa-navbar'
},
ngSimpaNav = {
  offsetOnResize: 0,
  globalWidth: 0,
  opt: '',
  init: function(params){
    this.opt = $.extend( defaults, params );
    ngSimpaNav.addBodyMargin();
    ngSimpaNav.onLoadAction();
    ngSimpaNav.resizeAction();
    ngSimpaNav.scrollAction();
  },
  navbarOffset: function(){
    return $(this.opt.navbar).offset().top;
  },
  addBodyMargin: function(){
    if( $(window).outerWidth() <= 980 ){
        $('body').css('margin', '0');
        $(this.opt.headerWrapperSelector).css('position', 'relative');
    }
  },
  onLoadAction: function(){
    var scrollTop = $(window).scrollTop();
    globalWidth = window.innerWidth || document.documentElement.clientWidth;


    if( $(this.opt.headerWrapperSelector).height() < scrollTop && globalWidth > 980 ){
        $('body').addClass('fixed');
        this.setCssStyles();
    } else {
        $('body').removeClass('fixed');
        this.removeCssStyles();
    }
    this.calculateElementHeight();
  },
  resizeAction: function(){
    var that = this;
    $(window).resize(function() {
        that.calculateElementHeight();
    });
  },
  scrollAction: function(){
    var that = this;
    var navbarOffset = this.navbarOffset();
    $(window).scroll(function(){
        var scrollTop = $(window).scrollTop();
        if( globalWidth > 1024 ){
            if( $(that.opt.headerWrapperSelector).height() ){
                if( $(that.opt.headerWrapperSelector).height() < scrollTop){
                    $('body').addClass('fixed');
                    that.setCssStyles();
                } else {
                    $('body').removeClass('fixed');
                    that.removeCssStyles();
                }
            }
        }
    });
  },
  calculateElementHeight: function(){
    globalWidth = window.innerWidth || document.documentElement.clientWidth;
    if( globalWidth <= 1024 && globalWidth >= 980 ){
        $('body').css('margin', '0');
        $(this.opt.headerWrapperSelector).css('position', 'relative');
        this.addIpadStyle();
    }else if( globalWidth < 980 ){
        $('body').css('margin', '0');
        $(this.opt.headerWrapperSelector).css('position', 'relative');
        this.removeIpadStyle();
    }else{
        var iHeaderHeight = parseInt( $(this.opt.headerWrapperSelector).outerHeight(), 10 );
        $('body').css('margin-top', iHeaderHeight);
        $(this.opt.headerWrapperSelector).css('position', 'fixed');
        this.removeIpadStyle();
    }
  },
  getFirstDiv: function(){
    return $(this.opt.headerWrapperSelector).next();
  },
  addIpadStyle: function(){
    var iPadCSS = {
        'position': 'fixed',
        'width': '100%',
        'z-index': 2147483640,
        'top': 0
    };
    var divAfterCSS = {
        'margin-top': '40px'
    };
    $(this.opt.navbar).css(iPadCSS);
    $(this.opt.headerWrapperSelector).css(divAfterCSS);
  },
  removeIpadStyle: function(){
    var navbarCSS = {
        'position': 'relative',
        'width': '100%',
        'z-index': 2147483640,
        'top': 'auto',
        'box-shadow': 'none'
    };
    var divAfterCSS = {
        'margin-top': 0
    };
    $(this.opt.navbar).css(navbarCSS);
    $(this.opt.headerWrapperSelector).css(divAfterCSS);
  },
  setCssStyles: function(){
    var navbarCSS = {
        'position': 'fixed',
        'width': '100%',
        'z-index': 2147483640,
        'top': 0,
        'box-shadow': '0 0 15px 0 #000'
    }
    var divAfterCSS = {
        'padding-top': $(this.opt.navbar).height()
    };
    $(this.opt.navbar).css(navbarCSS);
    this.getFirstDiv().css(divAfterCSS);
  },
  removeCssStyles: function(){
    var navbarCSS = {
        'position': 'relative',
        'width': '100%',
        'z-index': 2147483640,
        'top': 'auto',
        'box-shadow': 'none'
    };
    var divAfterCSS = {
        'padding-top': 0
    };
    $(this.opt.navbar).css(navbarCSS);
    this.getFirstDiv().css(divAfterCSS);
  }
};

window.ngSimpaNav = ngSimpaNav;

})();





