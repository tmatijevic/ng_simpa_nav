**Demo**: [ngSimpaNav](http://jsbin.com/acolid/1/edit) - ** Do not forget to click first on "Run with JS" located on output window **

ngSimpaNav
==========================

Really cool and responsive navigation with header above it. It is similiar to original bootstrap navigation, but with some more effects.

## Usage ##

Default options and initialisation:

```javascript
$(function(){
  ngSimpaNav.init({
    headerWrapperSelector: '.ng-simpa-navigation-header',
    navbar: '#ng-simpa-navbar'
  });
});
```

## Options ##

* __headerWrapperSelector__ - Main and only header on the top of the page
* __navbar__ - Main navigation which is used

## IMPORTANT NOTICE ##

__ALL__ siblings of the headerWrapperSelector must have background or background-image! This is important because if you don't set it - header will be visible when scrolling.

__Example__:

```css
.main-content { background: #fff; }
#footer { background: #fff; }
```

Above CSS is used in index.html example.

## Author ##

Tomislav Matijević

